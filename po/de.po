# GERMAN translation for Converter.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Jürgen Benvenuti <gastornis@posteo.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-25 11:47-0600\n"
"PO-Revision-Date: \n"
"Last-Translator: Jürgen Benvenuti <gastornis@posteo.org>\n"
"Language-Team: German <gnome-de@gnome.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.1.1\n"

#: data/io.gitlab.adhami3310.Converter.desktop.in:3
#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:5
#: converter/gtk/window.blp:9 converter/gtk/window.blp:42
msgid "Converter"
msgstr "Converter"

#: data/io.gitlab.adhami3310.Converter.desktop.in:4
msgid "Convert"
msgstr "Umwandeln"

#: data/io.gitlab.adhami3310.Converter.desktop.in:5
msgid "Convert and Manipulate images"
msgstr "Bilder umwandeln und bearbeiten"

#: data/io.gitlab.adhami3310.Converter.desktop.in:11
msgid "image;convert;converting;processing"
msgstr ""
"image;convert;converting;processing;Bild;umwandeln;Umwandlung;konvertieren;"
"verarbeiten;"

#: data/io.gitlab.adhami3310.Converter.gschema.xml:6
msgid "Shows less popular datatypes in the menu"
msgstr "Weniger gängige Datentypen im Menü anzeigen"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:6
msgid "Khaleel Al-Adhami"
msgstr "Khaleel Al-Adhami"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:7
msgid "Convert and manipulate images"
msgstr "Bilder umwandeln und bearbeiten"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:13
#, fuzzy
msgid ""
"A simple and easy-to-use Flatpak application built with libadwaita for "
"converting and manipulating images."
msgstr ""
"Eine einfache und leicht zu bedienende Flatpak-Anwendung zum Umwandeln und "
"Bearbeiten von Bildern, erstellt mit Libadwaita."

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:26
msgid "image"
msgstr "Bild"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:27
msgid "convert"
msgstr "Umwandeln"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:28
msgid "converting"
msgstr "Umwandlung läuft"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:29
msgid "processing"
msgstr "Verarbeitung läuft"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:45
msgid "Overview"
msgstr "Übersicht"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:65
msgid "Added batch processing and support for GIF, PDF, TIFF, and ICO."
msgstr ""

#: converter/file_chooser.py:67
#, python-brace-format
msgid "’{input_ext}’ is not supported"
msgstr "»{input_ext}« wird nicht unterstützt"

#: converter/file_chooser.py:90
msgid "Select an image"
msgstr "Ein Bild auswählen"

#: converter/file_chooser.py:97
msgid "Supported image files"
msgstr "Unterstützte Bilddateien"

#: converter/file_chooser.py:114
msgid "No file extension was specified"
msgstr "Es wurde keine Dateiendung angegeben"

#: converter/file_chooser.py:120
#, python-brace-format
msgid "’{file_ext}’ is of the wrong format"
msgstr "»{file_ext}« hat das falsche Format"

#: converter/file_chooser.py:129
msgid "Select output location"
msgstr "Ausgabeort auswählen"

#: converter/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Allgemein"

#: converter/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Tastenkürzel anzeigen"

#: converter/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Quit"
msgstr "Beenden"

#: converter/gtk/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Open Image"
msgstr "Bild öffnen"

#: converter/gtk/window.blp:53
msgid "_Open Image…"
msgstr "Bild ö_ffnen …"

#: converter/gtk/window.blp:62
msgid "…or just drop it"
msgstr "… oder einfach hier ablegen"

#: converter/gtk/window.blp:89
msgid "Drop the image to load it"
msgstr "Das Bild ablegen, um es zu laden"

#: converter/gtk/window.blp:107
msgid "Properties"
msgstr "Eigenschaften"

#: converter/gtk/window.blp:110
msgid "Image Type"
msgstr "Bildtyp"

#: converter/gtk/window.blp:117
msgid "Image Size"
msgstr "Bildgröße"

#: converter/gtk/window.blp:126
msgid "Options"
msgstr "Optionen"

#: converter/gtk/window.blp:158
#, fuzzy
msgid "Convert to"
msgstr "Umwandeln"

#: converter/gtk/window.blp:163
msgid "Compress as"
msgstr ""

#: converter/gtk/window.blp:176
msgid "More Options"
msgstr "Weitere Optionen"

#: converter/gtk/window.blp:185
msgid "_Convert"
msgstr "_Umwandeln"

#: converter/gtk/window.blp:205
msgid "Could not Load Image"
msgstr "Bild konnte nicht geladen werden"

#: converter/gtk/window.blp:206
msgid "This image could be corrupted or may use an unsupported file format."
msgstr ""
"Dieses Bild könnte beschädigt sein oder ein nicht unterstütztes Dateiformat "
"verwenden."

#: converter/gtk/window.blp:221
msgid "Quality"
msgstr "Qualität"

#: converter/gtk/window.blp:240
msgid "Background Color"
msgstr "Hintergrundfarbe"

#: converter/gtk/window.blp:248
msgid "DPI"
msgstr "DPI"

#: converter/gtk/window.blp:259
msgid "Scale Preserving Ratio"
msgstr "Seitenverhältnis beim Skalieren beibehalten"

#: converter/gtk/window.blp:262
msgid "Dimension"
msgstr "Abmessungen"

#: converter/gtk/window.blp:265 converter/gtk/window.blp:309
#: converter/gtk/window.blp:335
msgid "Width"
msgstr "Breite"

#: converter/gtk/window.blp:265 converter/gtk/window.blp:324
#: converter/gtk/window.blp:345
msgid "Height"
msgstr "Höhe"

#: converter/gtk/window.blp:270
msgid "Width in Pixels"
msgstr "Breite in Pixeln"

#: converter/gtk/window.blp:280
msgid "Height in Pixels"
msgstr "Höhe in Pixeln"

#: converter/gtk/window.blp:292
msgid "Resize"
msgstr "Größe ändern"

#: converter/gtk/window.blp:295
msgid "Filter"
msgstr "Filter"

#: converter/gtk/window.blp:301
msgid "Size"
msgstr "Größe"

#: converter/gtk/window.blp:304
msgid "Percentage"
msgstr "Prozent"

#: converter/gtk/window.blp:304
msgid "Exact Pixels"
msgstr "Genaue Pixelanzahl"

#: converter/gtk/window.blp:304
msgid "Minimum Pixels"
msgstr "Mindestpixelanzahl"

#: converter/gtk/window.blp:304
msgid "Maximum Pixels"
msgstr "Höchstpixelanzahl"

#: converter/gtk/window.blp:304
msgid "Ratio"
msgstr "Seitenverhältnis"

#: converter/gtk/window.blp:312
msgid "Pixels"
msgstr "Pixel"

#: converter/gtk/window.blp:312
msgid "Preserve Ratio"
msgstr "Seitenverhältnis beibehalten"

#: converter/gtk/window.blp:355
msgid "Width Percentage Scale"
msgstr "Prozentwert Breitenskalierung"

#: converter/gtk/window.blp:365
msgid "Height Percentage Scale"
msgstr "Prozentwert Höhenskalierung"

#: converter/gtk/window.blp:375
msgid "Ratio Width"
msgstr "Seitenverhältnis Breite"

#: converter/gtk/window.blp:385
msgid "Ratio Height"
msgstr "Seitenverhältnis Höhe"

#: converter/gtk/window.blp:405
msgid "Converting…"
msgstr "Umwandlung läuft …"

#: converter/gtk/window.blp:406
msgid "This could take a while."
msgstr "Das kann eine Weile dauern."

#: converter/gtk/window.blp:416 converter/window.py:531
msgid "Loading…"
msgstr "Wird geladen …"

#: converter/gtk/window.blp:422 converter/window.py:797
msgid "_Cancel"
msgstr ""

#: converter/gtk/window.blp:448
msgid "Open File…"
msgstr "Datei öffnen …"

#: converter/gtk/window.blp:455
msgid "Show Less Popular Datatypes"
msgstr "Weniger gängige Datentypen anzeigen"

#: converter/gtk/window.blp:467
msgid "Keyboard Shortcuts"
msgstr "Tastenkürzel"

#: converter/gtk/window.blp:472
msgid "About Converter"
msgstr "Info zu Converter"

#: converter/main.py:93
msgid "Code and Design Borrowed from"
msgstr "Code und Gestaltung entlehnt von"

#: converter/main.py:104
msgid "Sample Image from"
msgstr "Beispielbild von"

#: converter/main.py:143
#, fuzzy
msgid ""
"Jürgen Benvenuti <gastornis@posteo.org>\n"
"Irénée Thirion <irenee.thirion@e.email>\n"
"Mattia Borda <mattiagiovanni.borda@icloud.com>\n"
"Heimen Stoffels\n"
"Sergio <sergiovg01@outlook.com>\n"
"Åke Engelbrektson <eson@svenskasprakfiler.se>\n"
"Sabri Ünal <libreajans@gmail.com>\n"
"Azat Zinnetullin"
msgstr ""
"Jürgen Benvenuti <gastornis@posteo.org>\n"
"Irénée Thirion <irenee.thirion@e.email>\n"
"Mattia Borda <mattiagiovanni.borda@icloud.com>\n"
"Heimen Stoffels\n"
"Sergio <sergiovg01@outlook.com>"

#: converter/window.py:655 converter/window.py:738
#, fuzzy
msgid "Converting Cancelled"
msgstr "Umwandlung läuft …"

#: converter/window.py:790
#, fuzzy
msgid "Stop converting?"
msgstr "Umwandlung läuft"

#: converter/window.py:791
msgid "You will lose all progress."
msgstr ""

#: converter/window.py:798
msgid "_Stop"
msgstr ""

#: converter/window.py:831
msgid "Image converted"
msgstr "Bild wurde umgewandelt"

#: converter/window.py:832
msgid "Open"
msgstr "Öffnen"

#: converter/window.py:837
msgid "Error while processing"
msgstr "Fehler bei der Verarbeitung"

#: converter/window.py:863
msgid "Error copied to clipboard"
msgstr "Fehler in die Zwischenablage kopiert"

#: converter/window.py:867
msgid "_Copy to clipboard"
msgstr "In die Zwischenablage _kopieren"

#: converter/window.py:869
msgid "_Dismiss"
msgstr "_Verwerfen"

#~ msgid "Added AVIF and JXL support."
#~ msgstr "Unterstützung für AVIF und JXL hinzugefügt."

#~ msgid "Algorithms by"
#~ msgstr "Algorithmen von"

#~ msgid "Datatype"
#~ msgstr "Datentyp"

#~ msgid ""
#~ "Converter allows you to convert and manipulate a given image. It is a "
#~ "front-end for ImageMagick."
#~ msgstr ""
#~ "Converter ermöglicht Ihnen das Umwandeln und Bearbeiten eines bestimmten "
#~ "Bildes. Es ist ein Frontend für ImageMagick."

#~ msgid "Save Location"
#~ msgstr "Speicherort"

#~ msgid "(None)"
#~ msgstr "(Keiner)"

#~ msgid "Save location is missing."
#~ msgstr "Speicherort fehlt."

#~ msgid "Added \"Open With\" functionality."
#~ msgstr "»Öffnen mit«-Funktion hinzugefügt."

#~ msgid "Adjust DPI"
#~ msgstr "DPI anpassen"

#~ msgid "Initial Converter release"
#~ msgstr "Erste Veröffentlichung von Converter"
